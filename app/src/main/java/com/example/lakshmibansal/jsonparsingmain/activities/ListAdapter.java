package com.example.lakshmibansal.jsonparsingmain.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lakshmibansal.jsonparsingmain.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LAKSHMI BANSAL on 1/27/2015.
 */
public class ListAdapter extends BaseAdapter {
    List<String> data = new ArrayList<>();
    Context context;

    public ListAdapter(Context ctx, List<String> list) {
        this.context = ctx;
        this.data = list;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_item, null);
        TextView value = (TextView) view.findViewById(R.id.value);
        value.setText("NAME : " + data.get(position));
        return view;
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
