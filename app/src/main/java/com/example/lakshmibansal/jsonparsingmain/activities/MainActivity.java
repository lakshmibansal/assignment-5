package com.example.lakshmibansal.jsonparsingmain.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lakshmibansal.jsonparsingmain.R;
import com.example.lakshmibansal.jsonparsingmain.util.Data;
import com.example.lakshmibansal.jsonparsingmain.util.FacebookUtils;
import com.facebook.Session;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.lakshmibansal.jsonparsingmain.appConstants.AppConstants.*;


public class MainActivity extends Activity {
    Map<String, String> friendMap = new HashMap<>();
    ListView listview;
    ListAdapter adapter;
    Button showBtn;
    List<String> list;
    String[] details = new String[5];
    AsyncHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listview = (ListView) findViewById(R.id.list);
        showBtn = (Button) findViewById(R.id.show);
        client = new AsyncHttpClient();
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                client.get(PROFILE_URL + friendMap.get(parent.getItemAtPosition(position))+QUERY+Data.ACCESS_TOKEN, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String s) {
                        try {
                            JSONObject data = new JSONObject(s);
                            details[0] = data.getString("first_name");
                            details[1] = data.getString("last_name");
                            details[2] = data.getString("gender");
                            details[3] = data.getString("link");
                            Intent intent = new Intent(MainActivity.this, Details.class);
                            intent.putExtra("details", details);
                            startActivity(intent);
                        } catch (JSONException ex) {
                            Toast.makeText(MainActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        Toast.makeText(MainActivity.this, "fail " + s, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode,
                resultCode, data);
    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.show:
                getFriendList();
                break;
        }
    }

    public void getFriendList(){
        FacebookUtils.fbLogin(0,MainActivity.this);
        FacebookUtils.getUserData(0,MainActivity.this);
        client.get(URL_FRIENDS+ Data.ACCESS_TOKEN, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                try {
                    JSONObject data = new JSONObject(s);
                    JSONArray friendsArray = data.getJSONArray("data");
                    JSONObject friend;
                    for (int i = 0; i < friendsArray.length(); i++) {
                        friend = friendsArray.getJSONObject(i);
                        friendMap.put(friend.getString("name"), friend.getString("id"));
                    }
                    list = new ArrayList<>(friendMap.keySet());
                    adapter = new ListAdapter(MainActivity.this, list);
                    listview.setAdapter(adapter);
                    showBtn.setVisibility(View.INVISIBLE);

                } catch (JSONException ex) {
                    Toast.makeText(MainActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable throwable, String s) {

            }
        });
    }
}
