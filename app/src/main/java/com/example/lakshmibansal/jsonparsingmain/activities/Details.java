package com.example.lakshmibansal.jsonparsingmain.activities;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lakshmibansal.jsonparsingmain.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.example.lakshmibansal.jsonparsingmain.appConstants.AppConstants.PROFILE_URL;
import static com.example.lakshmibansal.jsonparsingmain.appConstants.AppConstants.URL_FRIENDS;


public class Details extends Activity {
    TextView firstName, lastName, gender, link;
    String[] details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        firstName = (TextView) findViewById(R.id.first_name);
        lastName = (TextView) findViewById(R.id.last_name);
        gender = (TextView) findViewById(R.id.gender);
        link = (TextView) findViewById(R.id.link);
        details = getIntent().getStringArrayExtra("details");
        firstName.setText("first name : "+details[0]);
        lastName.setText("last name : "+details[1]);
        gender.setText("gender : "+details[2]);
        link.setText(Html.fromHtml("<b><a href = \""+details[3]+"\">Click Here</a></b>"));
        link.setMovementMethod(LinkMovementMethod.getInstance());
    }

}
